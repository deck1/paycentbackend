from django.shortcuts import render
from rest_framework import generics,mixins
from rest_framework.parsers import JSONParser 
from rest_framework.decorators import api_view
from django.http.response import JsonResponse
import uuid 
# Create your views here.


@api_view(['GET','POST'])
def bloom(request):
    data={}
    data['code']='200'
    data['errormsg']='NA'
    data['transactionid']='banktx-'+uuid.uuid1().hex
    data['approvideid']='appro-'+uuid.uuid1().hex
    return JsonResponse({'result': data}, status=200)