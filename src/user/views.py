from django.shortcuts import render
from .models import *
from .serializers import *
from rest_framework import generics,mixins
from .suballlog import *
from .logsublogin import *
from merchant.models import EmailInvoiceLog
from merchant.serializers import EmailInvoiceLogSerializer

class CardApi(mixins.CreateModelMixin, generics.ListAPIView): 
    resource_name = 'cardlists'
    serializer_class= CardSerializer
    
    def get_queryset(self):
        return Card.objects.all() 
    
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class CardDetail(generics.RetrieveUpdateDestroyAPIView): 
    resource_name = 'cardlists'
    lookup_field  = 'id'
    serializer_class = CardSerializer

    def get_queryset(self):
        return Card.objects.all() 

class EmailInvoiceDetail(generics.ListAPIView): 
    resource_name = 'emailinvoicelogs'
    serializer_class = EmailInvoiceLogSerializer

    def get_queryset(self):
        invoiceid=self.request.GET['invoiceid']
        status=self.request.GET['status']
        return EmailInvoiceLog.objects.filter(invoiceid=invoiceid,statuscode=status)

class Authentication(generics.ListAPIView):
    resource_name = 'logauths'
    serializer_class = UserSerializer

    def get_queryset(self):
        try:
            user = self.request.GET['query[value1]']
            password=self.request.GET['query[value]']
            return User.objects.filter(loginid=user,password=password)
            pass
        except Exception as e:
            print(e)
       
