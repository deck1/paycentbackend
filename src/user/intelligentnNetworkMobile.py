from django.shortcuts import render
from .models import *
from .serializers import *
from rest_framework import generics,mixins
import requests
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework.decorators import api_view
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from base64 import b64decode
from base64 import b64encode
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
import os
import uuid 

class TransactionMobileApi(mixins.CreateModelMixin, generics.ListAPIView): 
    resource_name = 'transactions'
    serializer_class= TransactionSerializer

    def intelligentDecision(self,model):
        bankurl=''
        bankname=''
        bankid=999
        data=BankServices.objects.filter(service=model['cardtype']).all()
        for d in data:
            bankdata=Bank.objects.filter(bankid=int(d.bankid)).all()
            for bnkdata in bankdata:
                bankurl=bnkdata.url
                bankname=bnkdata.name
                bankid=bnkdata.bankid
                pass
            pass
        return bankurl,bankname,bankid

    def decrypt(self,message):
        try:
            path_private = 'static/merchant/private_Aarong.pem'
            script_dir = os.path.dirname(__file__)
            abs_file_path = os.path.join(script_dir, path_private)
            f = open(abs_file_path, 'r')
            key = RSA.importKey(f.read())
            cipher = PKCS1_OAEP.new(key, hashAlgo=SHA256)
            decrypted2_message = cipher.decrypt(b64decode(message))
            pass
        except Exception as e:
            print("error")
            print(e)
        return str(decrypted2_message,'utf-8')
    
    def ecrypt_bank(self,message):
        path_public ='static/bank/public_city.pem'
        script_dir = os.path.dirname(__file__)
        abs_file_path = os.path.join(script_dir, path_public)
        with open(abs_file_path, "rb") as key_file:
            public_key = serialization.load_pem_public_key(
                key_file.read(),
                backend=default_backend()
                )
            encrypted = public_key.encrypt(message.encode(),
             padding.OAEP(
                 mgf=padding.MGF1(algorithm=hashes.SHA256()),
                 algorithm=hashes.SHA256(),
                 label=None
                 ))
            return b64encode(encrypted)
    
    def get_queryset(self):
        return Transaction.objects.all() 
    
    def post(self, request, *args, **kwargs):
        try:
            cvv=request.data['cvv']
            cardno=request.data['cardno']
            expyy=request.data['expyy']
            expmm=request.data['expmm']
            cardnoArr = cardno.split('-')
            bankReq=request.data
            bankcardno=''
            for i in cardnoArr:
                bankcardno=bankcardno+i
                pass
            (bankurl,bankname,bankid)=self.intelligentDecision(request.data)
            request.data['cardno']=cardnoArr[0]+"-xxxxx-"+cardnoArr[len(cardnoArr)-1]
            request.data['cvv']=cvv
            request.data['expyy']=expyy
            request.data['expmm']=expmm
            request.data['bankid']=bankid
            request.data['bankname']=bankname
            request.data['transactionid']='tx-'+uuid.uuid1().hex
            request.data['approvedid']='NA'
            request.data['banktxid']='NA'
            request.data['status']='NA'
            request.data['comment']='NA'
            request.data['code']='40'
            insert=self.create(request, *args, **kwargs)
            payload={}
            payload['cvv']=self.ecrypt_bank(cvv)
            payload['cardno']=self.ecrypt_bank(bankcardno)
            payload['expmm']=self.ecrypt_bank(expmm)
            payload['expyy']=self.ecrypt_bank(expyy)
            payload['finalamount']=request.data['finalamount']
            result=requests.post(bankurl,data=payload)
            bankdata=result.json()['result']

            request.data['approvedid']=bankdata['approvideid']
            request.data['banktxid']=bankdata['transactionid']
            request.data['statuscode']=bankdata['code']
            request.data['errormsg']=bankdata['errormsg']
            return self.create(request, *args, **kwargs)
            pass
        except Exception as e:
            print(e)
        
        