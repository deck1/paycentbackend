import redis
from time import sleep
from threading import Thread
import json
from .models import AllLog
from datetime import datetime
from pytz import timezone 
from .externalurl import *

newhost=exnewhost
password=expassword
redis_r = redis.Redis(host=newhost, port=6379,db=0,password=password)
redis_p = redis_r.pubsub()
redis_p.subscribe("payallsub")

class alllog(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
        pass
    def run(self):
        while True:
            message=redis_p.get_message()
            encoding = 'utf-8'
            if(message):
                if(message['data'] != 1):
                    now = datetime.now(timezone("Asia/Dhaka"))
                    dtone=now.strftime("%d/%m/%y")
                    current_time_one = now.strftime("%H:%M:%S")
                    dic=str(message['data'],encoding)
                    res = json.loads(dic)
                    cnt=AllLog.objects.count()
                    AllLog.objects.create(dt=dtone,datetimestamp=now,time=current_time_one,id=cnt+1,userid=res['userid'],username=res['username'],userrole=res['userrole'],activitycat=res['activitycat'],activitysubcat=res['activitysubcat'],status=res['status'],statuscode=res['statuscode'],variablebefore=res['variablebefore'],variableafter=res['variableafter'],comment=res['comment'],reason=res['reason'],reasoncode=res['reasoncode'])
                    pass
                pass
        pass
    pass

alllog()
