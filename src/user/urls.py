from django.urls import path
from .views import *
from .intelligentnNetwork import *
from .intelligentnNetworkMobile import *

urlpatterns = [
    path(r'logauths',Authentication.as_view()),
    path(r'cardlists',CardApi.as_view(),name='personnel-create'),
    path(r'cardlists/<int:id>', CardDetail.as_view()),
    path(r'transactions', TransactionApi.as_view()),
    path(r'transactionsmobile', TransactionMobileApi.as_view()),
    path(r'emailinvoicelogs',EmailInvoiceDetail.as_view())
]
