from django.db import models

# Create your models here.


class LoginLog(models.Model):
    class Meta:
        db_table="login_log"
        pass
    datetimestamp=models.CharField(max_length=300)
    dt=models.CharField(max_length=300)
    time=models.CharField(max_length=300)
    userid=models.CharField(max_length=300)
    username=models.CharField(max_length=300)
    lat=models.CharField(max_length=300)
    lng=models.CharField(max_length=300)
    ip=models.CharField(max_length=300)
    inputuserid=models.CharField(max_length=300)
    inputpassword=models.CharField(max_length=300)
    userrole=models.CharField(max_length=300)
    activitytype=models.CharField(max_length=300)
    status=models.CharField(max_length=300)
    statuscode=models.CharField(max_length=300)
    variablebefore=models.CharField(max_length=1000)
    variableafter=models.CharField(max_length=1000)
    comment=models.CharField(max_length=1000)
    reason=models.CharField(max_length=1000)
    reasoncode=models.CharField(max_length=1000)

class AllLog(models.Model):
    class Meta:
        db_table="all_log"
        pass
    datetimestamp=models.CharField(max_length=300)
    dt=models.CharField(max_length=300)
    time=models.CharField(max_length=300)
    userid=models.CharField(max_length=300)
    username=models.CharField(max_length=300)
    userrole=models.CharField(max_length=300)
    activitycat=models.CharField(max_length=300)
    activitysubcat=models.CharField(max_length=300)
    status=models.CharField(max_length=300)
    statuscode=models.CharField(max_length=300)
    variablebefore=models.CharField(max_length=1000)
    variableafter=models.CharField(max_length=1000)
    comment=models.CharField(max_length=1000)
    reason=models.CharField(max_length=1000)
    reasoncode=models.CharField(max_length=1000)

class User(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="tbl_user"
    name=models.CharField(max_length=300)
    loginid=models.CharField(max_length=300)
    password=models.CharField(max_length=300)
    merchantid=models.CharField(max_length=300)

    def __str__(self):
        return self.name

class UserRole(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="user_role"
        pass

    loginid=models.CharField(max_length=300)
    role=models.CharField(max_length=300)

    def __str__(self):
        return self.loginid


class Bank(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="bank"
    bankid = models.IntegerField()
    name=models.CharField(max_length=300)
    rate=models.FloatField()
    url=models.CharField(max_length=300)

class BankServices(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="bankservices"
    bankid = models.IntegerField()
    service=models.CharField(max_length=300)

class Card(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="card"
        
    cardid = models.BigIntegerField()
    name=models.CharField(max_length=300)
    bankid = models.BigIntegerField()
    rate = models.FloatField()
    bankname=models.CharField(max_length=300)
    cardtype=models.CharField(max_length=300)
    prefix =models.CharField(max_length=300)
    imagename=models.CharField(max_length=300)
    imageurl=models.CharField(max_length=1000)
    length= models.FloatField()
    cardstructure=models.CharField(max_length=300)
    cvvpattern=models.CharField(max_length=300)
    cvvlength =models.CharField(max_length=300)


class Transation(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="transaction"
    
    datetimestamp=models.CharField(max_length=300)
    finalamount=models.CharField(max_length=300)
    cardno=models.CharField(max_length=300)
    bankid=models.IntegerField()
    transactionid=models.CharField(max_length=300)
    approvedid=models.CharField(max_length=300)
    banktxid=models.CharField(max_length=300)
    bankname=models.CharField(max_length=300)
    paymenttype=models.CharField(max_length=300)
    errormsg=models.CharField(max_length=300)
    status=models.CharField(max_length=300)
    statuscode=models.CharField(max_length=300)
    comment=models.CharField(max_length=300)
    paycentpct=models.FloatField(max_length=300)
    bankpct=models.FloatField(max_length=300)
    merchantpct=models.FloatField(max_length=300)
    priority=models.FloatField(max_length=300)
    cardtype=models.CharField(max_length=300)
    merchantid=models.CharField(max_length=300)
    invoiceid=models.CharField(max_length=300)
