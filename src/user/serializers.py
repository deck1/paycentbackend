from rest_framework import serializers
from .models import *
import os
import uuid

class BankServicesSerializer(serializers.ModelSerializer):
    class Meta:
        model=BankServices
        fields = '__all__'

class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model=Card
        fields=('cardid','name','bankid','rate','bankname','cardtype','prefix','imagename','imageurl','length','cardstructure','cvvpattern','cvvlength')


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model=Transation
        fields = '__all__'

  

class UserSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField(source='get_token')
    role = serializers.SerializerMethodField(source='get_role')

    def get_token(self,obj):

        access_token_payload = {
            'user_id': obj.loginid,
            }
        access_token = str(uuid.uuid4().hex)
        return  access_token
    
    def get_role(self,obj):
        user=UserRole.objects.filter(loginid=obj.loginid)
        role=''
        for u in user:
            role=u.role
            pass
        return role
        
    class Meta:
        model=User
        fields=('id','name','loginid','password','token','role','merchantid')
