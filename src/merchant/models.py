from django.db import models

# Create your models here.


class EmailInvoiceLog(models.Model):
    class Meta:
        db_table="invoiceemail_log"
        pass
    datetimestamp=models.CharField(max_length=300)
    dt=models.CharField(max_length=300)
    time=models.CharField(max_length=300)
    userid=models.CharField(max_length=300)
    username=models.CharField(max_length=300)
    userrole=models.CharField(max_length=300)
    recievername=models.CharField(max_length=300)
    recieveremail=models.CharField(max_length=300)
    transactionid=models.CharField(max_length=300)
    unit=models.CharField(max_length=300)
    invoiceid=models.CharField(max_length=300)
    transactionstatus=models.CharField(max_length=300)
    transactionstatuscode=models.CharField(max_length=300)
    subject=models.CharField(max_length=300)
    message=models.CharField(max_length=300)
    amount=models.CharField(max_length=300)
    status=models.CharField(max_length=300)
    statuscode=models.CharField(max_length=300)
    comment=models.CharField(max_length=1000)
    reason=models.CharField(max_length=1000)
    reasoncode=models.CharField(max_length=1000)


class Merchant(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="merchant"
        pass
    merchantid=models.CharField(max_length=300)
    name=models.CharField(max_length=300)
    rate=models.FloatField()
    payday=models.IntegerField()
    mobileno=models.CharField(max_length=300)
    email=models.CharField(max_length=300)
    division=models.CharField(max_length=300)
    district=models.CharField(max_length=300)
    thana=models.CharField(max_length=300)
    zip=models.CharField(max_length=300)
    country=models.CharField(max_length=300)
    logo=models.CharField(max_length=10000)


