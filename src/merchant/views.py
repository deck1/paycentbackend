from django.shortcuts import render
from .models import *
from rest_framework.decorators import api_view
from django.http.response import JsonResponse
from .emailsend import *


# Create your views here.

@api_view(['GET'])
def test(request):
    return JsonResponse({'result': 'test'}, status=200)