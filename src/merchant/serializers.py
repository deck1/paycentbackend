from rest_framework import serializers
from .models import *
import os
import uuid



class EmailInvoiceLogSerializer(serializers.ModelSerializer):
    class Meta:
        model=EmailInvoiceLog
        fields = '__all__'