from user.externalurl import *
import redis
from time import sleep
from threading import Thread
import json
from email.mime.image import MIMEImage
from django.core.mail import EmailMultiAlternatives
from .models import EmailInvoiceLog,Merchant
from datetime import datetime
from pytz import timezone 
import uuid
import qrcode
import os 
import base64



emailurl=emailurl
newhost=exnewhost
password=expassword
redis_r = redis.Redis(host=newhost, port=6379,db=0,password=password)
redis_p = redis_r.pubsub()
redis_p.subscribe("invoiceemailrecieved")

class EmailSub(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
        pass
    def run(self):
        while True:
            message=redis_p.get_message()
            encoding = 'utf-8'
            if(message):
                if(message['data'] != 1):
                    invoiceid=str(uuid.uuid4().hex)
                    now = datetime.now(timezone("Asia/Dhaka"))
                    dtone=now.strftime("%d/%m/%y")
                    current_time_one = now.strftime("%H:%M:%S")
                    dic=str(message['data'],encoding)
                    res = json.loads(dic)
                    cnt=EmailInvoiceLog.objects.count()
                    merchantdata=Merchant.objects.filter(merchantid=res['merchantid']).all()
                    for data in merchantdata:
                        EmailInvoiceLog.objects.create(
                        id=cnt+1,
                        dt=dtone,
                        datetimestamp=now,
                        time=current_time_one,
                        userid=data.merchantid,
                        username=data.name,
                        userrole=res['role'],
                        recievername=res['username'],
                        recieveremail=res['email'],
                        transactionid='N/A',
                        unit=res['unit'],
                        invoiceid=invoiceid,
                        transactionstatus='Yet to pay',
                        transactionstatuscode='100',
                        subject=res['subject'],
                        message=res['description'],
                        amount=res['amount'],
                        status='Email to send',
                        statuscode='100',
                        comment='N/A',
                        reason='Email to send',
                        reasoncode='100',
                        )

                        emailurlto=emailurl+invoiceid
                        message_html = """<meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><html><head><style>p.ex1{padding: 10px 10px 10px 10px;} img{display: block;margin-left: auto;margin-right: auto;} hr{border: 0.6px solid #EAECEE;}  button{background-color: rgb(0, 84, 147); border: none; color: white; padding: 15px 32px; text-align: center;text-decoration:none; display:inline-block; font-size:16px; border-radius: 12px;}</style></head><body>"""
                        
                        
                        message_html = message_html+"""\
                                        <hr>
                                        <p style="text-align:center"><span style="color: #2C3E50; font-weight: 900;font-family:Arial, sans-serif, 'Open Sans';font-size: 36px">Dear """ + res['username'] + """,</span></p>"""

                        message_html = message_html+"""\
                                         <p style="text-align:center"><span style="color: #2C3E50; font-family:Arial, sans-serif, 'Open Sans';font-size: 18px"> """ +  res['description'] + """<br><br> To pay from web please enter your invoice id : """+  invoiceid + """<br><br><br><br><button><a style="color: white;font-size:16px;" href="""+emailurlto+""">Pay from web</a></button></span></p></body>"""
                        
                        message_html = message_html+ """<p class="ex1"><img style="width:30%;" src="cid:qrcode"></p></html></body>"""
                        subject = res['subject']
                        message = res['description']
                        msg = EmailMultiAlternatives(subject,message_html,from_email='paycent.live@gmail.com',to=[res['email']])
                        msg.mixed_subtype = 'related'
                        msg.attach_alternative(message_html, "text/html")
                        qr = qrcode.QRCode(
                             version=1,
                             error_correction=qrcode.constants.ERROR_CORRECT_L,
                             box_size=10,
                             border=4,
                             )
                             
                        qr.add_data(res['amount'])
                        qr.make(fit=True)
                        image = qr.make_image(fill_color="black", back_color="white")
                        path='data'+'/'+invoiceid+'.png'
                        dir=os.path.dirname(__file__)
                        filename = os.path.join(dir, path)
                        image.save(filename)
                        imagefilename=invoiceid+'.png'
                        with open(filename, 'rb') as f:
                            img = MIMEImage(f.read())
                            img.add_header('Content-ID', '<qrcode>')
                            img.add_header('Content-Disposition', 'inline', filename=imagefilename)
                        msg.attach(img)
                        msg.send()
                        cnte=EmailInvoiceLog.objects.count()
                        nowtwo = datetime.now(timezone("Asia/Dhaka"))
                        dttwo=nowtwo.strftime("%d/%m/%y")
                        current_time_two = now.strftime("%H:%M:%S")
                        EmailInvoiceLog.objects.create(
                            id=cnte+1,
                            dt=dttwo,
                            datetimestamp=nowtwo,
                            time=current_time_two,
                            userid=data.merchantid,
                            username=data.name,
                            userrole=res['role'],
                            recievername=res['username'],
                            recieveremail=res['email'],
                            transactionid='N/A',
                            unit=res['unit'],
                            invoiceid=invoiceid,
                            transactionstatus='Yet to pay',
                            transactionstatuscode='100',
                            subject=res['subject'],
                            message=res['description'],
                            amount=res['amount'],
                            status='Email Sent',
                            statuscode='150',
                            comment='N/A',
                            reason='Email Sent',
                            reasoncode='150',
                        )
                
                        pass
                    
                    
                    pass
                pass
            sleep(2)
        pass
    pass

EmailSub()
